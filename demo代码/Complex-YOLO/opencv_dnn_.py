import cv2
from complexYOLO import *

import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#model = load_model("config/complex_yolov3.cfg", "weights/yolov3_ckpt_epoch-298.pth").to(device)
model = ComplexYOLO().to(device)
dummy_input = Variable(torch.randn(1, 3, 512, 1024)).to(device)
torch.onnx.export(model, dummy_input, "torch.onnx")


"""

net_path = "E:/3D_object_dec/Yes/Complex-YOLO-master/yolov2-kitti_50000_1.weights"
net_cfg = "E:/3D_object_dec/Yes/Complex-YOLO-master/complex-yolo.cfg"
net = cv2.dnn.readNetFromDarknet(net_cfg,net_path)
"""
print('OK')
