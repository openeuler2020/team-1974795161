# 16-皇德耀世

### 介绍
TOPIC_ID:16, TEAM_ID:1974795161, TEAM_NAME:皇德耀世.

### 功能描述

基于雷达点云数据应用 SFA3D 网络实现实时的 3D 车辆检测，网络将雷达点云转化为BEV（鸟瞰图）的形式作为输入，将 3D 锚框位置与角度作为输出。
在 demo 中将锚框标注在 RGB 图像中作为最终输出，并在鸟瞰图中也标记出锚框位置。
最终结果以视频的形式呈现，能够较好的使用 3D 锚框检测出车辆的姿态。

将complex-yolo网络成功使用opencv中dnn模块进行推导，实现基于点云的3D目标检测，网络拥有较快的速度实用性极强，
使用opencv推导后，减小了网络对特定库的依赖性。


### demo 说明
运行opencv推导后demo

python eval.py

#### 构建数据：
python kitti_dataset.py
#### 运行 demo：
python demo_2_sides.py --gpu_idx 0 --peak_thresh 0.2

#### demo演示：

![demo1](https://img-blog.csdnimg.cn/20210420201758144.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTE4NDAyNQ==,size_16,color_FFFFFF,t_70#pic_center)



![在这里插入图片描述](https://img-blog.csdnimg.cn/20210420201758122.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NTE4NDAyNQ==,size_16,color_FFFFFF,t_70#pic_center)